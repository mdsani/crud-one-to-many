<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <!--bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>

<!--php code-->
<?php
require_once '../../vendor/autoload.php';
use Sani\Brands;
$brand = new Brands;
$brands = $brand->show($_GET['id']);
?>

<!--php error code-->
<?php if(isset($_SESSION['errors'])){ foreach($_SESSION['errors'] as $errors){ ?>
<p class="text-center"><?= $errors ?></p>
<?php }}unset($_SESSION['errors']); ?>

<!--form-->
<h3 class="text-center">Update Data</h3>

<form action="update.php" method="POST" class="container" style="max-width: 500px;">
  <div class="form-group">
    <input type="hidden" name="id" value="<?= $brands['id'] ?>">
  </div>
  <div class="form-group">
    <label for="Name">Brands Name</label>
    <input type="text" name="name" value="<?= $brands['name'] ?>" class="form-control" placeholder="Ente Brands Name">
  </div>
  <div class="form-group">
    <label for="Description">Description</label>
    <input type="text" name="description" value="<?= $brands['description'] ?>" class="form-control" placeholder="Ente Description">
  </div>
  <div class="form-group text-center">
    <button type="submit" onclick="return confirm('Are you sure want to Update')" class="btn btn-primary">Update</button>
    <a class="btn btn-primary" href="index.php" role="button">List</a>
  </div>
</form>


<!--bootstrap js-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>