<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <!--bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>

<!--php code-->
<?php
require_once '../../vendor/autoload.php';
use Sani\Products;
$product = new Products;
$productsobj = $product->index();
$products = $productsobj['products'];
$totalproduct = $productsobj['products_count'];
?>

<!--php error code-->
<?php if(isset($_SESSION['message'])){ ?>
<p class="text-center"><?= $_SESSION['message'] ?></p>
<?php }unset($_SESSION['message']); ?>

<!--table-->
<h3 class="text-center">All Information</h3>

<div class="text-right container mb-2">
    <a class="btn btn-primary" href="create.php" role="button">Add</a>  
    <a class="btn btn-primary" href="trash.php" role="button">Trash</a>  
</div>

<table class="table table-striped container">
  <thead>
    <tr>
      <th scope="col">SL#</th>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col" class="text-center" style="width: 300px;">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $sl=0; foreach($products as $product){ ?>
        <tr>
            <td><?= ++$sl; ?></td>
            <td><?= $product['title'] ?></td>
            <td><?= $product['price'] ?></td>
            <td class="text-center" style="width: 300px;">
                <a class="btn btn-primary" href="show.php?id=<?= $product['id'] ?>" role="button">Show</a>
                <a class="btn btn-primary" href="edit.php?id=<?= $product['id'] ?>" role="button">Edit</a>
                <a class="btn btn-primary" onclick="return confirm('Are you sure want to delete')" href="destroy.php?id=<?= $product['id'] ?>" role="button">Delete</a>
            </td>
        </tr>
    <?php } ?>
  </tbody>
</table>


<div class="container">
<?php $totalPage = ceil($totalproduct/Products::PAGINATE_PER_PAGE); 
for($i= 1; $i<=$totalPage; $i++){ ?>

<a style="border: 1px solid #DCDCDC; padding:10px; " href="index.php?page=<?= $i ?>"><?= $i ?></a>

<?php } ?>
</div>




<!--bootstrap js-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>