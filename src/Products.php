<?php
namespace Sani;
use PDO;
use PDOException;

class Products{

    public $conn = '';
    public $brand_id = '';
    public $title = '';
    public $price = '';
    public $image = '';
    const PAGINATE_PER_PAGE = 5;

    public function __construct()
    {
        try{
            session_start();
            $this->conn = new PDO
            ("mysql:host=localhost;dbname=brands","root","admin");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function index()
    {
        $perPage = self::PAGINATE_PER_PAGE;
        if(isset($_GET['page'])){
            $pageNumber = $_GET['page'];
            $offset = ($pageNumber-1) * $perPage;
            $sql = "SELECT * FROM `products` where id_delete = 0 order by id asc limit $perPage OFFSET $offset"; 
        }else{
            $sql = "SELECT * FROM `products` where id_delete = 0 order by id asc limit $perPage";
        }
        $stmt = $this->conn->query($sql);

        $countproductsSql = "SELECT COUNT(*) as totalItem FROM `products`";
        $countproductsStmt = $this->conn->query($countproductsSql);
        $productsCount = $countproductsStmt->fetchColumn();

        return [
            'products' => $stmt->fetchAll(),
            'products_count' => $productsCount
        ];
    }

    public function SetData(array $data = [])
    {
        $errors = [];
        if($_FILES['image']['name']){
            //image vadidation
            $imageTmpName = $_FILES['image']['tmp_name'];
            $orginalName = $_FILES['image']['name'];
            $imageSize = $_FILES['image']['size'];

            //image type validation
            $imageExplode = explode('.', $orginalName);
            $imageName = end($imageExplode);
            $imageType = ['png', 'jpg', 'jpeg'];
            if(!in_array($imageName, $imageType)){
                $errors[] = 'Type Required';
            }

            //image size validation
            if($imageSize > 55048576){
                $errors = 'Size Required';
            }
        }

        //form validation
        if(array_key_exists('brand_id', $data) && !empty($data['brand_id'])){
            $this->brand_id = $data['brand_id'];
        }else{
            $errors[] = 'Brand Id  required';
        }
        if(array_key_exists('title', $data) && !empty($data['title'])){
            $this->title = $data['title'];
        }else{
            $errors[] = 'title required';
        }
        if(array_key_exists('price', $data) && !empty($data['price'])){
            $this->price = $data['price'];
        }else{
            $errors[] = 'Price required';
        }

        if(count($errors)){
            $_SESSION['errors'] = $errors;
            header('location: '.$_SERVER['HTTP_REFERER']);
        }else{
            //image move
            if($_FILES['image']['name']){
                $imageNameUnic = time().'__'.$orginalName;
                $this->image = $imageNameUnic;
                move_uploaded_file($imageTmpName, '../../assets/image/product/'.$imageNameUnic);
                }
            return $this;
            }
            
        
    }

    public function brand()
    {
        $sql = "SELECT  id, name FROM `brands`";
        $stmt = $this->conn->query($sql);
        $brands = $stmt->fetchAll();
        return ['brands'=>$brands];
        
    }

    public function store()
    {
        try{
        $query ="INSERT INTO products(brand_id, title, price, image) VALUES(:brand_id, :title , :price, :image)";
        $stmt =$this->conn->prepare($query);
        $stmt->execute(array(
        ':brand_id' => $this->brand_id,
        ':title' => $this->title,
        ':price' =>(int) $this->price,
        ':image' => $this->image
        ));
        $_SESSION['message'] = 'Successfull Create !';
        header('Location:index.php');
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function show($id)
    {
        $query = "SELECT * FROM products where id=".$id;
        $stmt = $this->conn->query($query);
        return $stmt->fetch();
    }

    public function update($id)
    {
        try{
            $query ="UPDATE products SET brand_id=:brand_id, title=:title, price=:price, image=:image where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':brand_id' => $this->brand_id,
                ':title' => $this->title,
                ':price' =>(int) $this->price,
                ':image' => $this->image
            ));
            $_SESSION['message'] = 'Successfully Updated !';
            header('Location:index.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
            
    }

    public function delete($id)
    {
        try {
            $query ="delete from  where id=".$id;
            $stmt = $this->conn->query($query);
            $stmt ->execute();
            $_SESSION['message'] = 'Successfull Delete !';
            header('Location:index.php');
        }catch(PDOException $e){
            echo $e->getMessage();
        }     
    }

    public function destroy($id)
    {
        try{
            $query ="UPDATE products SET id_delete=:id_delete where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':id_delete' => 1
            ));
            $_SESSION['message'] = 'Successfully Delete !';
            header('Location:index.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public function trash()
    {
        $query = "SELECT * FROM products where id_delete=1";
        $stmt = $this->conn->query($query);
        return $stmt->fetchAll();
    }

    public function restore($id)
    {
        try{
            $query ="UPDATE products SET id_delete=:id_delete where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':id_delete' => 0
            ));
            $_SESSION['message'] = 'Successfully Restore !';
            header('Location:trash.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    

}

?>