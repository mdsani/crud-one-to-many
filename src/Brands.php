<?php
namespace Sani;
use PDO;
use PDOException;

class Brands{

    public $conn = '';
    public $name = '';
    public $description = '';
    const PAGINATE_PER_PAGE = 5;

    public function __construct()
    {
        try{
            session_start();
            $this->conn = new PDO
            ("mysql:host=localhost;dbname=brands","root","admin");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function index()
    {
        $perPage = self::PAGINATE_PER_PAGE;
        if(isset($_GET['page'])){
            $pageNumber = $_GET['page'];
            $offset = ($pageNumber-1) * $perPage;
            $sql = "SELECT * FROM `brands` where id_delete = 0 order by id asc limit $perPage OFFSET $offset"; 
            // echo $offset;
        }else{
            $sql = "SELECT * FROM `brands` where id_delete = 0 order by id asc limit $perPage";
        }
        $stmt = $this->conn->query($sql);

        $countbrandsSql = "SELECT COUNT(*) as total_category FROM `brands`";
        $countbrandsStmt = $this->conn->query($countbrandsSql);
        $brandsCount = $countbrandsStmt->fetchColumn();

        return [
            'brands' => $stmt->fetchAll(),
            'brands_count' => $brandsCount
        ];
    }

    public function SetData(array $data = [])
    {
        $errors = [];
        if(array_key_exists('name', $data) && !empty($data['name'])){
            $this->name = $data['name'];
        }else{
            $errors[] = 'Name required';
        }
        if(array_key_exists('description', $data) && !empty($data['description'])){
            $this->description = $data['description'];
        }else{
            $errors[] = 'Description required';
        }

        if(count($errors)){
            $_SESSION['errors'] = $errors;
            header('location: '.$_SERVER['HTTP_REFERER']);
        }else{
            return $this;
        }
    }

 

    public function store()
    {
        try{
        $query ="INSERT INTO brands(name, description) VALUES(:name , :description)";
        $stmt =$this->conn->prepare($query);
        $stmt->execute(array(
        ':name' => $this->name,
        ':description' => $this->description
        ));
        $_SESSION['message'] = 'Successfull Create !';
        header('Location:index.php');
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function show($brandId)
    {
        $sql = "SELECT * FROM `products` where brand_id=".$brandId;
        $stmt = $this->conn->query($sql);
        $brands = $stmt->fetchAll();
        return ['products'=>$brands];
    }

    public function update($id)
    {
        try{
            $query ="UPDATE brands SET name=:name, description=:description where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':name' => $this->name,
                ':description' => $this->description
            ));
            $_SESSION['message'] = 'Successfully Updated !';
            header('Location:index.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
            
    }

    public function delete($id)
    {
        try {
            $query ="delete from  where id=".$id;
            $stmt = $this->conn->query($query);
            $stmt ->execute();
            $_SESSION['message'] = 'Successfull Delete !';
            header('Location:index.php');
        }catch(PDOException $e){
            echo $e->getMessage();
        }     
    }

    public function destroy($id)
    {
        try{
            $query ="UPDATE brands SET id_delete=:id_delete where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':id_delete' => 1
            ));
            $_SESSION['message'] = 'Successfully Delete !';
            header('Location:index.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public function trash()
    {
        $query = "SELECT * FROM brands where id_delete=1";
        $stmt = $this->conn->query($query);
        return $stmt->fetchAll();
    }

    public function restore($id)
    {
        try{
            $query ="UPDATE brands SET id_delete=:id_delete where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':id_delete' => 0
            ));
            $_SESSION['message'] = 'Successfully Restore !';
            header('Location:trash.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    

}

?>